﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	
	private Rigidbody2D rigidBody;
	private bool facingRight = true;
	private float moveX;
	public float Speed = 0f;
	
	void Start () {
		rigidBody = GetComponent<Rigidbody2D> ();
	}
	
	void FixedUpdate () {
		if(facingRight){
			moveX = 1;
		}else{
			moveX = -1;
		}
		rigidBody.velocity = new Vector2(moveX*Speed, rigidBody.velocity.y);
	}
	
	void OnTriggerEnter2D(Collider2D col){
		Debug.Log ("Trigger entered");
		if (col.tag == "trigger1") {
			facingRight = true;
		} else if (col.tag == "trigger2") {
			Debug.Log ("Trigger2!");
			facingRight = false;
		}
	}
}