﻿using UnityEngine;
using System.Collections;

public class Playercontroller : MonoBehaviour {
	public float Speed = 10f;
	private float movex = 10f;
	private float movey = 10f;
	private GameObject startImage;
	
	private void FixedUpdate () {
		movex = Input.GetAxis ("Horizontal");
		movey = Input.GetAxis ("Vertical");
		GetComponent<Rigidbody2D>().velocity = new Vector2 (movex * Speed, movey * Speed);
	}
	
	private void OnTriggerEnter2D(Collider2D objectPlayerCollideWith)
	{
		if(objectPlayerCollideWith.tag == "USD"){
			//playerHealth += healthperFruit;
			//healthText.text = "+" + healthperFruit + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollideWith.gameObject.SetActive (false);
			//SoundController.Instance.PlaySingle (fruitSound1, fruitSound2);
		}
	}
}